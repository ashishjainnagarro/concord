
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CoreApiService } from 'src/app/infrastructure/core/services/core-api.service';
import { catchError, map } from 'rxjs/operators';
import { IMapGlobal } from 'src/app/shared/map-global.model';

@Injectable()
export class MapGlobalService extends CoreApiService<IMapGlobal> {
    private route: string;
    constructor() {
        super();
    }

    getMapGlobal = (): Observable<IMapGlobal[]> => {
        this.route = 'mapGlobal';
        return this.getAll(this.route).pipe(catchError(() => {
            return this.getJSON();
        }));
    }

    public getJSON(): Observable<any> {
        return this.http.get('./assets/map-global.json').pipe(
            map(response => { console.log(response); return response; })
        );
    }

}
