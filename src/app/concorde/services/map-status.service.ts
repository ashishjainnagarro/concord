
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CoreApiService } from 'src/app/infrastructure/core/services/core-api.service';
import { catchError, map } from 'rxjs/operators';
import { IMapStatus } from 'src/app/shared/map-status.model';

@Injectable()
export class MapStatusService extends CoreApiService<IMapStatus> {
    private route: string;
    constructor() {
        super();
    }

    getMapStatues = (): Observable<IMapStatus[]> => {
        this.route = 'mapStatuses';
        return this.getAll(this.route).pipe(catchError(() => {
            return this.getJSON();
        }));
    }

    public getJSON(): Observable<any> {
        return this.http.get('./assets/map-status.json').pipe(
            map(response => { console.log(response); return response; })
        );
    }

}
