
import { Injectable } from '@angular/core';
import { of, Observable } from 'rxjs';
import { MapStatuses } from 'src/app/shared/map-status.enum';
import { IMapAdmin } from 'src/app/shared/map-admin.model';
import { CoreApiService } from 'src/app/infrastructure/core/services/core-api.service';
import { catchError, map } from 'rxjs/operators';

@Injectable()
export class MapAdminService extends CoreApiService<IMapAdmin> {
    private route: string;
    constructor() {
        super();
    }

    getMapper = (): Observable<IMapAdmin[]> => {
        this.route = 'mapAdmin';
        return this.getAll(this.route).pipe(catchError(() => {
            return this.getJSON();
        }));
    }

    public getJSON(): Observable<any> {
        return this.http.get('./assets/map-admin.json').pipe(
            map(response => { console.log(response); return response; })
        );
    }

}
