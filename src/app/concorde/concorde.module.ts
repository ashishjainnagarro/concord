import { NgModule } from '@angular/core';
import { ConcordeRoutingModule } from './concorde-routing.module';
import { ConcordeComponent } from './concorde.component';
import { SharedModule } from '../shared/shared.module';
import { MapAdminService } from './services/map-admin.service';
import { MapStatusService } from './services/map-status.service';
import { MapGlobalService } from './services/map-global.service';

@NgModule({
  declarations: [ConcordeComponent],
  imports: [
    ConcordeRoutingModule,
    SharedModule,
  ],
  providers: [MapAdminService, MapStatusService, MapGlobalService]
})
export class ConcordeModule { }
