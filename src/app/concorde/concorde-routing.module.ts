import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConcordeComponent } from './concorde.component';

const routes: Routes = [
    { path: '', component: ConcordeComponent, pathMatch: 'full' },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ConcordeRoutingModule { }
