import { Component, OnInit } from '@angular/core';
import { SelectItem, LazyLoadEvent } from 'primeng/api';
import { IMapAdmin } from '../shared/map-admin.model';
import { MapAdminService } from './services/map-admin.service';
import { IMapStatus } from '../shared/map-status.model';
import { MapStatusDescription } from '../shared/map-status.enum';
import { IMapGlobal } from '../shared/map-global.model';
import { MapStatusService } from './services/map-status.service';
import { MapGlobalService } from './services/map-global.service';
import { IGridColumn } from '../shared/grid-column.model';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-concorde',
  templateUrl: './concorde.component.html',
  styleUrls: ['./concorde.component.css']
})
export class ConcordeComponent implements OnInit {

  mapAdmin: IMapAdmin[];
  mapper: SelectItem[];
  selectedMapper: IMapAdmin;

  mapStatuses: IMapStatus[];
  mapStatusUI: SelectItem[];
  selectedMapStatus: IMapStatus;

  clientTypes: SelectItem[] = [];

  mapGlobal: IMapGlobal[];
  loading: boolean;
  // multiSortMeta = [{ field: 'AAANumber', order: 1 }, { field: 'ClientName', order: -1 }];

  mapGlobalGridColumns: IGridColumn[] = [
    { field: 'AAANumber', header: 'OB10 Number' },
    { field: 'ClientName', header: 'Client' },
    { field: 'StagingIdentifier', header: 'Staging ID' },
    { field: 'DateReceived', header: 'Date Received' },
    { field: 'SupplierBuyerName', header: 'Buyer' },
    { field: 'CountryCode', header: 'Country' },
    { field: 'EntryType', header: 'Entry Type' },
    { field: 'FileType', header: 'File Type' },
    { field: 'MapContact', header: 'Map Contact' },
    { field: 'SIContact', header: 'SI Contact' },
    { field: 'RevisedDueDate', header: 'Last Status Change' },
  ];

  constructor(private mapAdminService: MapAdminService,
    private mapStatusService: MapStatusService,
    private mapGlobalService: MapGlobalService) { }

  ngOnInit() {

    this.loadMapGlobalLazy(null);
    this.mapAdminService.getMapper().subscribe(val => {
      this.mapAdmin = val.map(x => {
        return {
          name: x.name,
          initial: x.initial,
          email: x.email,
          ppIdentifier: x.ppIdentifier,
          hdtIdentifier: x.hdtIdentifier,
          superUserIdentifier: x.superUserIdentifier,
          teamIdentifier: x.teamIdentifier,
          multiTeamIdentifier: x.multiTeamIdentifier,
          mappingType: x.mappingType,
          overrideUser: x.overrideUser

        };
      });

      this.mapper = this.mapAdmin.map(x => {
        return {
          label: x.name,
          value: x
        };
      });


      this.mapper = [
        { label: '< Select Mapper >', value: null }, ...this.mapper
      ];

    });

    this.mapStatusService.getMapStatues().subscribe(val => {
      this.mapStatuses = [...val];
      this.mapStatusUI = val.map(x => {
        return {
          label: MapStatusDescription.get(x.status),
          value: x
        };
      });

      this.mapStatusUI = [{ label: '< Select Map Status >', value: null }, ...this.mapStatusUI];
    });
  }

  loadMapGlobalLazy(event: LazyLoadEvent) {
    //event.first = First row offset
    //event.rows = Number of rows per page
    //event.sortField = Field name to sort in single sort mode
    //event.sortOrder = Sort order as number, 1 for asc and -1 for dec in single sort mode
    //multiSortMeta: An array of SortMeta objects used in multiple columns sorting. Each SortMeta has field and order properties.
    //filters: Filters object having field as key and filter value, filter matchMode as value
    //globalFilter: Value of the global filter if available
    //do a request to a remote datasource using a service and return the cars that match the lazy load criteria
    this.loading = true;
    setTimeout(() => {
      this.mapGlobalService.getMapGlobal().subscribe(x => {
        this.mapGlobal = x;

        const clientTypesMap = new Map();


        this.clientTypes.push({
          label: '< Select >',
          value: null
        });
        this.mapGlobal.forEach(y => {
          if (!clientTypesMap.has(y.ClientType)) {
            clientTypesMap.set(y.ClientType, null);
            this.clientTypes.push({
              label: y.ClientType,
              value: y.ClientType
            });
            // return {
            //   label: y.ClientType,
            //   value: y
            // };
          }
        });

      });
      this.loading = false;
    }, 1000);


  }
}
