import { Injector } from '@angular/core';

export class CoreInjector {
    private static injector: Injector;

    static setInjector = (injector: Injector) => {
        CoreInjector.injector = injector;
    }
    static getInjector = (): Injector => {
        return CoreInjector.injector;
    }
}
