import { InjectionToken } from '@angular/core';

export const ApplicationId = new InjectionToken<string>('Application Id');
export const ApiBaseUrl = new InjectionToken<string>('Api Base Url');
export const ApiVersion = new InjectionToken<string>('Api Version');
