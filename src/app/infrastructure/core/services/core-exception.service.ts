import { ErrorHandler, Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';

@Injectable()
export class CoreException implements ErrorHandler {
    handleError(error: HttpErrorResponse) {
        console.log(error);
        return throwError(error.message);
    }
}
