import { Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CoreInjector } from './core-injector.service';
import { BaseResource } from 'src/app/shared/base-resource.model';
import { Observable, throwError } from 'rxjs';
import { CoreException } from './core-exception.service';
import { catchError } from 'rxjs/operators';
import { ApplicationId, ApiBaseUrl, ApiVersion } from './injection.token';

@Injectable()
export class CoreApiService<T extends BaseResource> {
    private applicationId: string;
    private apiBaseUrl: string;
    private apiVersion: string;

    protected http: HttpClient;
    protected exception: CoreException;

    constructor() {
        const injector: Injector = CoreInjector.getInjector();

        this.applicationId = injector.get(ApplicationId);
        this.apiBaseUrl = injector.get(ApiBaseUrl);
        this.apiVersion = injector.get(ApiVersion);
        this.http = injector.get(HttpClient);
        this.exception = injector.get(CoreException);
    }

    getAll(routeString: string): Observable<T[]> {
        const url = `${this.apiBaseUrl}${this.apiVersion}/${routeString}`;
        return this.http
            .get<T[]>(url)
            .pipe(
                catchError((error) => {
                    this.exception.handleError(error);
                    return throwError(error);
                }));
    }
}
