import { NgModule, ErrorHandler } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { CoreApiService } from './services/core-api.service';
import { CoreException } from './services/core-exception.service';
import { HttpClientModule } from '@angular/common/http';
import { environment } from 'src/environments/environment';

import { ApplicationId, ApiBaseUrl, ApiVersion } from './services/injection.token';

@NgModule({
  declarations: [],
  imports: [
    SharedModule,
    HttpClientModule,
  ],
  providers: [
    CoreApiService,
    CoreException,
    { provide: ErrorHandler, useClass: CoreException },
    { provide: ApplicationId, useValue: environment.applicationId },
    { provide: ApiBaseUrl, useValue: environment.apiBaseUrl },
    { provide: ApiVersion, useValue: environment.apiVersion },
  ]
})
export class CoreModule { }
