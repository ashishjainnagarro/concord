import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConcordeMappingReportsComponent } from './concorde-mapping-reports.component';

const routes: Routes = [
    { path: '', component: ConcordeMappingReportsComponent, pathMatch: 'full' },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ConcordeMappingReportsRoutingModule { }
