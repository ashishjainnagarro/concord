import { NgModule } from '@angular/core';
import { ConcordeMappingReportsRoutingModule } from './concorde-mapping-reports-routing.module';
import { ConcordeMappingReportsComponent } from './concorde-mapping-reports.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [ConcordeMappingReportsComponent],
  imports: [
    SharedModule,
    ConcordeMappingReportsRoutingModule
  ]
})
export class ConcordeMappingReportsModule { }
