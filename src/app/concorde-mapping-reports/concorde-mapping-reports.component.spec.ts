import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConcordeMappingReportsComponent } from './concorde-mapping-reports.component';

describe('ConcordeMappingReportsComponent', () => {
  let component: ConcordeMappingReportsComponent;
  let fixture: ComponentFixture<ConcordeMappingReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConcordeMappingReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConcordeMappingReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
