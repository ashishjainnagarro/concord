import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: '/', pathMatch: 'full' },
  {
    path: 'concorde',
    loadChildren: () => import('./concorde/concorde.module').then(m => m.ConcordeModule)
  },
  {
    path: 'concorde-mapping',
    loadChildren: () => import('./concorde-mapping/concorde-mapping.module').then(m => m.ConcordeMappingModule)
  },
  {
    path: 'concorde-mapping-reports',
    loadChildren: () => import('./concorde-mapping-reports/concorde-mapping-reports.module').then(m => m.ConcordeMappingReportsModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
