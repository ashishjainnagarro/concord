import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConcordeMappingComponent } from './concorde-mapping.component';

describe('ConcordeMappingComponent', () => {
  let component: ConcordeMappingComponent;
  let fixture: ComponentFixture<ConcordeMappingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConcordeMappingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConcordeMappingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
