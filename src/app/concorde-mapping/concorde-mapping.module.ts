import { NgModule } from '@angular/core';
import { ConcordeMappingRoutingModule } from './concorde-mapping-routing.module';
import { ConcordeMappingComponent } from './concorde-mapping.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [ConcordeMappingComponent],
  imports: [
    SharedModule,
    ConcordeMappingRoutingModule
  ]
})
export class ConcordeMappingModule { }
