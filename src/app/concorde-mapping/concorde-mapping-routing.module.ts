import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConcordeMappingComponent } from './concorde-mapping.component';

const routes: Routes = [
    { path: '', component: ConcordeMappingComponent, pathMatch: 'full' },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ConcordeMappingRoutingModule { }
