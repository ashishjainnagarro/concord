export enum MapStatus {
    forHTD = 1,
    new = 2,
    wip = 3,
    queriesLogSent = 4,
    waitingForInfo = 5,
    ppRequest = 6,
    html = 7,
    readyToLtp = 8,
    ltp = 9,
    forSI = 10,
}

export const MapStatusDescription: Map<MapStatus, string> = new Map<MapStatus, string>([
    [MapStatus.forHTD, 'FOR HTD'],
    [MapStatus.new, 'NEW'],
    [MapStatus.wip, 'WIP'],
    [MapStatus.queriesLogSent, 'QUERIES LOG SENT'],
    [MapStatus.waitingForInfo, 'WAITING FOR INFO'],
    [MapStatus.ppRequest, 'PP REQUEST'],
    [MapStatus.html, 'HTML'],
    [MapStatus.readyToLtp, 'READY TO LTP'],
    [MapStatus.ltp, 'LTP'],
    [MapStatus.forSI, 'FOR SI'],
]);

export const MapStatuses: MapStatus[] = [
    MapStatus.forHTD,
    MapStatus.html,
    MapStatus.new,
    MapStatus.ppRequest,
    MapStatus.queriesLogSent,
    MapStatus.readyToLtp,
    MapStatus.forSI,
    MapStatus.waitingForInfo,
    MapStatus.wip,
    MapStatus.ltp,
];
