export enum EntryType {
    map = 1,
    support = 2,
    rework = 3,
}

export const EntryTypeDescription: Map<EntryType, string> = new Map<EntryType, string>([
    [EntryType.map, 'MAP'],
    [EntryType.support, 'SUPPORT'],
    [EntryType.rework, 'REWORK'],
]);
