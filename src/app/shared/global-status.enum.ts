export enum GlobalStatus {
    received = 1,
    wip = 2,
    rtt = 3,
    live = 4,
    archived = 5,
    discontinued = 6,
}

export const GlobalStatusDescription: Map<GlobalStatus, string> = new Map<GlobalStatus, string>([
    [GlobalStatus.received, 'RECEIVED'],
    [GlobalStatus.wip, 'WIP'],
    [GlobalStatus.rtt, 'RTT'],
    [GlobalStatus.live, 'LIVE'],
    [GlobalStatus.archived, 'ARCHIVED'],
    [GlobalStatus.discontinued, 'DISCONTINUED'],
]);
