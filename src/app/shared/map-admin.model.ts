import { MappingType } from './mapping-type.enum';
import { TeamIdentifier } from './team-identifier.enum';
import { BaseResource } from './base-resource.model';

export interface IMapAdmin extends BaseResource {
    name?: string;
    initial?: string;
    email?: string;
    ppIdentifier?: number;
    hdtIdentifier?: number;
    superUserIdentifier?: number;
    teamIdentifier?: TeamIdentifier;
    multiTeamIdentifier?: string;
    mappingType?: MappingType;
    overrideUser?: string;
}
