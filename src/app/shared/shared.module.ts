import { NgModule } from '@angular/core';
import { InputTextModule } from 'primeng/inputtext';
import { SidebarModule } from 'primeng/sidebar';
import { CheckboxModule } from 'primeng/checkbox';
import { RadioButtonModule } from 'primeng/radiobutton';
import { ButtonModule } from 'primeng/button';
import { TabViewModule } from 'primeng/tabview';
import { CodeHighlighterModule } from 'primeng/codehighlighter';
import { DropdownModule } from 'primeng/dropdown';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { TableModule } from 'primeng/table';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    InputTextModule,
    SidebarModule,
    CheckboxModule,
    RadioButtonModule,
    ButtonModule,
    TabViewModule,
    CodeHighlighterModule,
    DropdownModule,
    ScrollPanelModule,
    TableModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [
    CommonModule,
    SidebarModule,
    InputTextModule,
    CheckboxModule,
    RadioButtonModule,
    ButtonModule,
    TabViewModule,
    CodeHighlighterModule,
    DropdownModule,
    ScrollPanelModule,
    TableModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class SharedModule { }
