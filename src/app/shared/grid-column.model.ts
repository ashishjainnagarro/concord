export interface IGridColumn {
    field: string;
    header: string;
}
