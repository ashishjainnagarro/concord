export enum MappingType {
    buyer = 1,
    supplier = 2,
}

export const MappingTypeDescription: Map<MappingType, string> = new Map<MappingType, string>([
    [MappingType.buyer, 'B'],
    [MappingType.supplier, 'S'],
]);
