export enum SupportStatus {
    new = 1,
    wip = 2,
    withOB10Support = 3,
    ppRequest = 4,
    rework = 5,
    resolvedOrLtp = 6,
}

export const SupportStatusDescription: Map<SupportStatus, string> = new Map<SupportStatus, string>([
    [SupportStatus.new, 'NEW'],
    [SupportStatus.wip, 'WIP'],
    [SupportStatus.withOB10Support, 'WITH OB10 SUPPORT'],
    [SupportStatus.ppRequest, 'PP REQUEST'],
    [SupportStatus.rework, 'REWORK'],
    [SupportStatus.resolvedOrLtp, 'RESOLVED/LTP'],
]);
