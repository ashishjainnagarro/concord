import { BaseResource } from './base-resource.model';

export interface IMapGlobal extends BaseResource {
    AAANumber: string;
    ClientType: string;
    ClientName: string;
    StagingIdentifier: string;
    DateReceived: string;
    DueDate: string;
    SupplierBuyerName: string;
    PPRequestDate: string;
    SSContact: string;
    MapContact: string;
    PPContact: string;
    SupportContact: string;
    EntryType: string;
    Status: string;
    PPStatus: string;
    GlobalStatus: string;
    StatusDateTime: Date;
    CountryCode: string;
    FileType: string;
    LTPDate: Date;
    MappingTime: Date;
    CurrentNoteDate: Date;
    CurrentNote: string;
    BuyerSignOffNotifier: string;
    SupportTicketNumber: string;
    ReworkType: string;
    BonusPoints: string;
    TestHTMLSent: string;
    SIContact: string;
    SIStatus: string;
    ExpectedLiveDate: string;
    Test: string;
    RevisedDueDate: Date;
    ReworkReason: string;
}
