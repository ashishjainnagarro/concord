export enum CountryCode {
    uk = 1,
    us = 2,
    nl = 3,
    fr = 4,
    sw = 5,
    de = 6,
    es = 7,
    be = 8,
}

export const CountryCodeDescription: Map<CountryCode, string> = new Map<CountryCode, string>([
    [CountryCode.uk, 'UK'],
    [CountryCode.us, 'US'],
    [CountryCode.nl, 'NL'],
    [CountryCode.fr, 'FR'],
    [CountryCode.sw, 'SW'],
    [CountryCode.de, 'DE'],
    [CountryCode.es, 'ES'],
    [CountryCode.be, 'BE'],
]);
