export enum PPStatus {
    requestMade = 1,
    wip = 2,
    waitingForInfo = 3,
    complete = 4,
}

export const PPStatusDescription: Map<PPStatus, string> = new Map<PPStatus, string>([
    [PPStatus.requestMade, 'REQUEST MADE'],
    [PPStatus.wip, 'WIP'],
    [PPStatus.waitingForInfo, 'WAITING FOR INFO'],
    [PPStatus.complete, 'COMPLETE'],
]);
