export enum TeamIdentifier {
    map = 1,
    sales = 2,
    leftOb10 = 3,
    support = 4,
}

export const TeamIdentifierDescription: Map<TeamIdentifier, string> = new Map<TeamIdentifier, string>([
    [TeamIdentifier.map, 'Map'],
    [TeamIdentifier.sales, 'Sales'],
    [TeamIdentifier.leftOb10, 'Left OB10'],
    [TeamIdentifier.support, 'Support'],
]);
