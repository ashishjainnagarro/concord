import { MapStatus } from './map-status.enum';
import { BaseResource } from './base-resource.model';

export interface IMapStatus extends BaseResource {
    status: MapStatus;
}
